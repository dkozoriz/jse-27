package ru.t1.dkozoriz.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.enumerated.Status;
import ru.t1.dkozoriz.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    public TaskStartByIndexCommand() {
        super("task-start-by-index", "start task by index.");
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @Nullable final String userId = getUserId();
        getTaskService().changeStatusByIndex(userId, index, Status.IN_PROGRESS);
    }

}
