package ru.t1.dkozoriz.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.api.repository.IUserRepository;
import ru.t1.dkozoriz.tm.model.User;

import java.util.List;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Nullable
    public User findByLogin(@Nullable final String login) {
        if (login == null) return null;
        return findAll()
                .stream()
                .filter(u -> login.equals(u.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    public User findByEmail(@Nullable final String email) {
        if (email == null) return null;
        return findAll()
                .stream()
                .filter(u -> email.equals(u.getEmail()))
                .findFirst()
                .orElse(null);
    }

    public boolean isLoginExist(@NotNull final String login) {
        return findAll()
                .stream()
                .anyMatch(u-> login.equals(u.getLogin()));
    }

    public boolean isEmailExist(@NotNull final String email) {
        return findAll()
                .stream()
                .anyMatch(u-> email.equals(u.getEmail()));
    }

}