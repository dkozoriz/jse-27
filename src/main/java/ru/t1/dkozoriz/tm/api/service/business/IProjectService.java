package ru.t1.dkozoriz.tm.api.service.business;

import ru.t1.dkozoriz.tm.model.business.Project;

public interface IProjectService extends IBusinessService<Project> {

}