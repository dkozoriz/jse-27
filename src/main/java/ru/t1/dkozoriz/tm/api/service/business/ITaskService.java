package ru.t1.dkozoriz.tm.api.service.business;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.dkozoriz.tm.model.business.Task;

import java.util.List;

public interface ITaskService extends IBusinessService<Task> {

    @NotNull
    List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    Task create(@Nullable String userId, @Nullable String name, @Nullable String description, @Nullable String projectId);

}